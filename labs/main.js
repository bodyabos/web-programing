let statusDisplay = document.querySelector("#statusDisplay");
let searchButton = document.querySelector("#searchButton");
var fromNumber = document.getElementById("from");
var toNumber = document.getElementById("to");
// Создаем поток
let worker = new Worker("js/PrimeWorker.js");
function doSearch() {
    // Отключаем кнопку запуска вычислений, чтобы пользователь не мог
    // запускать несколько процессов поиска одновременно
    searchButton.disabled = true;

    // Получаем начальное и конечное число диапазона поиска
    fromNumber = document.getElementById("from").value;
    toNumber = document.getElementById("to").value;

    // Подключаем функцию к событию onMessage, чтобы получать
    // сообщения от потока
    worker.onmessage = receivedWorkerMessage;
    worker.onerror = workerError;

    worker.postMessage(
        {
            from: fromNumber,
            to: toNumber
        }
    );

    // Информируем пользователя, что вычисления выполняются
    statusDisplay.innerHTML = "Фоновый поток ищет простые числа (от " +
        fromNumber + " до " + toNumber + ") ...";
}

function receivedWorkerMessage(event) {
    var message = event.data;

    if (message.messageType == "PrimeList") {
        // Отображаем список в соответствующей области страницы
        var primes = message.data;

        var primeList = "";
        for (var i = 0; i < primes.length; i++) {
            primeList += primes[i];
            if (i != primes.length - 1) primeList += ", ";
        }

        var primeContainer = document.getElementById("primeContainer");
        primeContainer.innerHTML = primeList;

        if (primeList.length == 0) {
            statusDisplay.innerHTML = "Ошибка поиска.";
        } else {
            statusDisplay.innerHTML = "Простые числа найдены!";
            notifyMe("Все числа найдены!");
        }
        searchButton.disabled = false;
    } else if (message.messageType == "Progress") {
        statusDisplay.innerHTML = message.data + "% выполнено ...";
    }
}

function workerError(error) {
    statusDisplay.innerHTML = error.message;
}

function cancelSearch() {
    worker.terminate();
    worker = null;
    statusDisplay.innerHTML = "Поток остановлен.";
    searchButton.disabled = false;
}

function startFrom() {
    fromNumber = document.getElementById("from");
    if (localStorage.getItem("lastNumber")) {
        fromNumber.value = localStorage.getItem("lastNumber");
        doSearch();
    } else {
        throw "LocalStorage недоступен";
    }
}


function notifyMe(why) {
    // Проверка поддержки браузером уведомлений
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
    }

    // Проверка разрешения на отправку уведомлений
    else if (Notification.permission === "granted") {
        // Если разрешено, то создаем уведомление
        var notification = new Notification("Hi there! " + why);
    }

    // В противном случае, запрашиваем разрешение
    else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
            // Если пользователь разрешил, то создаем уведомление
            if (permission === "granted") {
                var notification = new Notification("Hi there! " + why);
            }
        });
    }
}


// Set the name of the hidden property and the change event for visibility
var hidden, visibilityChange;
if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support
    hidden = "hidden";
    visibilityChange = "visibilitychange";
} else if (typeof document.msHidden !== "undefined") {
    hidden = "msHidden";
    visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
    hidden = "webkitHidden";
    visibilityChange = "webkitvisibilitychange";
}

// If the page is hidden, pause the video;
// if the page is shown, play the video
let timerStart, timerEnd, interval;

function handleVisibilityChange() {
    if (document[hidden]) {
        timerStart = new Date();
        interval = setInterval(function () {
            checkTime(timerStart, new Date())
        }, 1000);
    } else {
        clearInterval(interval);
        document.title = 'Oh hi Mark';
    }
}

function checkTime(start, now) {
    console.log(now, start);
    if (now - start > 5000) {
        console.log("OK, away");
        document.body.style.backgroundColor = "gray";
        document.title = 'YOU ARE AWAY';
        notifyMe("Get back please");
        clearInterval(interval);
    } else {
        console.log("not really");
    }
}

// Warn if the browser doesn't support addEventListener or the Page Visibility API
if (typeof document.addEventListener === "undefined" || hidden === undefined) {
    console.log("This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.");
} else {
    // Handle page visibility change
    document.addEventListener(visibilityChange, handleVisibilityChange, false);
}

// Сохраняем элемент в котором страница отображает результат
var result;

window.onload = function() {
    result = document.getElementById('result');

    // Если функциональность геолокации доступна,
    // пытаемся определить координаты посетителя
    if (navigator.geolocation) {
        // Передаем две функции
        navigator.geolocation.getCurrentPosition(
            geolocationSuccess, geolocationFailure);

        // Выводим результат
        result.innerHTML = "Поиск начался";
    }
    else {
        // Выводим результат
        result.innerHTML = "Ваш браузер не поддерживает геолокацию";
    }
};
//
// function geolocationSuccess(position) {
//     result.innerHTML = "Последний раз вас засекали здесь: " +
//         position.coords.latitude + ", " + position.coords.longitude;
//     console.log(position.coords.latitude + ", " + position.coords.longitude);
// }

function geolocationFailure(positionError) {
    if(positionError == 1) {
        result.innerHTML = "Вы решили не предоставлять данные о своем местоположении, " +
            "но это не проблема. Мы больше не будем запрашивать их у вас.";
    }
    else if(positionError == 2) {
        result.innerHTML = "Проблемы с сетью или нельзя связаться со службой определения " +
            "местоположения по каким-либо другим причинам.";
    }
    else if(positionError == 3) {
        result.innerHTML = "He удалось определить местоположение "
            + "в течение установленного времени. ";

    }
    else {
        result.innerHTML = "Загадочная ошибка.";
    }
    goToDefaultLocation();
}

var result;
var map;

result = document.getElementById('result');
window.onload = function() {

    // Устанавливаем некоторые параметры карты. В данном примере
    // устанавливаются начальный уровень масштабирования и тип карты.
    // Информацию о других параметрах см. в документации по Google Maps.
    var myOptions = {
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    // Создаем карту, используя установленные выше параметры
    map = new google.maps.Map(document.getElementById("mapSurface"), myOptions);

    // Пытаемся определить местоположение пользователя
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            geolocationSuccess, geolocationFailure);

        result.innerHTML = "Поиск завершен";
    }
    else {
        result.innerHTML = "Ваш браузер не поддерживает геолокацию";
        goToDefaultLocation();
    }
};
function geolocationSuccess(position) {
    // Преобразуем местоположение в объект LatLng
    console.log(position.coords.latitude + ", " + position.coords.longitude);
    var location = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);

    // Отображаем эту точку на карте
    map.setCenter(location);
    // Создаем всплывающее информационное окно и устанавливаем
    // его текст и положение на карте.
    var infowindow = new google.maps.InfoWindow();
    infowindow.setContent("Вы находитесь где-то в этом районе.");
    infowindow.setPosition(location);

    // Отображаем всплывающее окно
    infowindow.open(map);

    result.innerHTML = "Местоположение отмечено на карте.";
}
function goToDefaultLocation() {
    var spot = new google.maps.LatLng(55.24172, 32.62669739);
    map.setCenter(spot);
}
