<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
</head>
<body>
<?php
session_start();
require_once 'session.php';
?>



<?php if (Session::has('user')) : ?>
    <h1><a href="logout.php">Logout (<?= Session::get('user'); ?>)</a></h1>
    <h1><a href="deleteAccount.php">Delete account</a></h1>
    <h1><a href="changeAccount.php">Change account</a></h1>
    <h1><a href="changePassword.php">Change password</a></h1>
<?php else : ?>
    <h1><a href="login.php">Login</a></h1>
    <h1><a href="register.php">Register</a></h1>
<?php endif; ?>

<br/>
</body>
</html>