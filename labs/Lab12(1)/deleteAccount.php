<?php
session_start();
require_once 'session.php';

$servername = "localhost";
$usernamedb = "root";
$passworddb = "";
$db = "lab_12";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$db", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
    echo "Connection failed: " . $e->getMessage();
}
$username = Session::get('user');
$conn->query("DELETE FROM users where username = '{$username}'");
Session::delete('user');
header('location: index.php?msg=Your account deleted');