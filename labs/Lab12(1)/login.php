<?php
require_once('LoginForm.php');
require_once('Password.php');
require_once('Session.php');

session_start();
if (Session::has('user')){
    header('location: index.php');
}

$servername = "localhost";
$usernamedb = "root";
$passworddb = "";
$db = "lab_12";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$db", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
    echo "Connection failed: " . $e->getMessage();
}

if ($_POST) {
    $form = new LoginForm($_POST);
    $username = $form->getUsername();
    $password = new Password( $form->getPassword() );

    $res = $conn->query("SELECT * FROM users WHERE username = '{$username}' AND password = '{$password}' LIMIT 1");
    $res = $res->fetch();
    if (!$res) {
        echo 'No such user found';
    } else {
        $user = $res['username'];
        $role = $res['role'];
        Session::set('user', $user);
        Session::set('role', $role);
        header('location: index.php?msg=You have been logged in');
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>

</head>
<body>
<form method="post" id="form">
    Username: <input type="text" name="username" required/> <br/><br/>
    Password: <input type="password" name="password" required/> <br/><br/>
    <input type="submit"/>
</form>
</body>
</html>