<?php
require_once('Password.php');
require_once('Session.php');

session_start();

if (!Session::has('user')){
    header('location: index.php');
}

if($_POST){
    $servername = "localhost";
    $usernamedb = "root";
    $passworddb = "";
    $db = "lab_12";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$db", $usernamedb, $passworddb);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
        echo "Connection failed: " . $e->getMessage();
    }

    $currnetusername = Session::get('user');

    $currentPassword='';

    $query = "SELECT * FROM users WHERE username = '{$currnetusername}'";
    $result = $conn->query($query);
    while ($res = $result->fetch(PDO::FETCH_NUM)) {
        $currentPassword = $res[3];
    }
    $newPassword = new Password($_POST['new']);
//    var_dump($currentPassword);
//    var_dump(new Password($_POST['password']));
    $postPass = new Password($_POST['current']);
    if($currentPassword === $postPass->getHashedPassword()){
        $conn->query("UPDATE users SET password = '{$newPassword}' where username = '{$currnetusername}'");
        header('location: index.php?msg=Your password has been changed');
    }
    else{
        echo 'wrong current password';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Change data</title>
</head>
<body>
<form method="post">
    Current password <input type="password" name="current" required/> <br/><br/>
    New password <input type="password" name="new" required/> <br/><br/>
    <input type="submit"/>
</form>
</body>
</html>