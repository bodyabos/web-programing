let primes = [];
onmessage = function (event) {
    findPrime(event.data.from, event.data.to);
};

function findPrime(a, b) {
    primes = [];
    for (let i = a; i <= b; i++) {
        let start = performance.now();
        let notPrime = false;
        for (let j = 2; j <= i; j++) {
            if (i % j === 0 && j !== i) {
                notPrime = true;
            }
        }
        if (notPrime === false) {
            let stop = performance.now();
            primes.push(new Prime(i, stop - start));
        }
        postMessage(['in progress', primes]);
    }
    postMessage(['done', primes]);
}

function Prime(number, time) {
    this.number = number;
    this.time = time
}