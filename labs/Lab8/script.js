let a, b, stoped = false;
let aInput = document.getElementById('a');
let bInput = document.getElementById('b');
let start = document.getElementById('start');
let stop = document.getElementById('stop');
let container = document.getElementById('numbers');
let status = document.getElementById('status');

start.addEventListener('click', function () {
    if (!stoped) {
        doSearch();
    } else {
        restart();
        stoped = false;
    }
});

let worker = new Worker('worker.js');

stop.addEventListener('click', function () {
    status.innerHTML = 'work stopped';
    start.disabled = false;
    worker.terminate();
    worker = null;
    stoped = true;
    stop.disabled = true;
});

document.addEventListener('visibilitychange', function () {
    let interval = setInterval(function () {
        if (document.hidden) {
            let notification = new Notification('alert', {
                body: 'we are looking for you'
            });
        }
        clearInterval(interval);
    }, 6000);
});

window.onload = function () {
    stop.disabled = true;
    window.applicationCache.addEventListener('updateready', function () {
        if (window.applicationCache.status === window.applicationCache.UPDATEREADY) {
            window.applicationCache.swapCache();
            if (confirm('a new version of cache is available. load it?')) {
                window.location.reload();
            }
        }
    });
    navigator.geolocation.getCurrentPosition(function (position) {
        document.getElementById('geolocation').innerHTML = position.coords.latitude + ', ' + position.coords.longitude;
    });
    restart();
};

function restart() {
    if (localStorage.getItem('primes') !== null) {
        stop.disabled = false;
        status.innerHTML = 'in progress';
        aInput.value = localStorage.getItem('a');
        bInput.value = localStorage.getItem('b');
        let data = JSON.parse(localStorage['primes']);
        for (let i = 0; i < data.length; i++) {
            let tmp = data[i].number.toString() + ' ' + data[i].time.toString() + '\n';
            container.innerHTML += tmp;
        }
        if (worker === null) {
            worker = new Worker('worker.js');
        }
        worker.onmessage = receivedWorkerMessage;
        worker.postMessage({
            from: data[data.length - 1].number,
            to: +bInput.value,
        });
    }
}

function doSearch() {
    start.disabled = true;
    stop.disabled = false;
    status.innerHTML = 'in progress';
    a = +aInput.value;
    b = +bInput.value;
    localStorage.setItem('a', a);
    localStorage.setItem('b', b);
    worker.onmessage = receivedWorkerMessage;
    worker.postMessage({
        from: a,
        to: b,
    });
}

function receivedWorkerMessage(event) {
    let data = event.data;
    if (data[0] === 'in progress') {
        localStorage.removeItem('primes');
        localStorage.setItem('primes', JSON.stringify(data[1]));
    } else {
        localStorage.clear();
        let numbers = data[1];
        for (let i = 0; i < numbers.length; i++) {
            let tmp = numbers[i].number.toString() + ' ' + numbers[i].time.toString() + '\n';
            container.innerHTML += tmp;
            start.disabled = false;
        }
        stop.disabled = true;
        status.innerHTML = 'work done';
        Notification.requestPermission(
            function (permission) {
                if (permission !== "granted")
                    return false;
            }
        ).then(function () {
            let notification = new Notification('alert', {
                body: 'Numbers were found'
            });
        });
    }
}