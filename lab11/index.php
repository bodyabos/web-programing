<?php @session_start();

$LangArray = array("ru", "ua", "en");

$DefaultLang = "ua";
if(@$_SESSION['NowLang']) {

    if(!in_array($_SESSION['NowLang'], $LangArray)) {

        $_SESSION['NowLang'] = $DefaultLang;
    }
}
else {
    $_SESSION['NowLang'] = $DefaultLang;
}

$language = addslashes($_GET['lang']);

setcookie("language", $language, time()+ (183* 24* 60 * 60));

if($language) {

    if(!in_array($language, $LangArray)) {

        $_SESSION['NowLang'] = $DefaultLang;
    }
    else {

        $_SESSION['NowLang'] = $language;
    }
}
$_SESSION['login'] = $login;

$CurentLang = addslashes($_SESSION['NowLang']);
include_once ("lang/lang.".$CurentLang.".php");

?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <title><?php echo $Lang['title']; ?></title>
</head>
<body>

<form action="login.php" method="POST" enctype="multipart/form-data">

<table>

    <tr>
       <td colspan="2"><h1><?php echo $Lang['nameoflab']; ?></h1></td>
    </tr>
    <tr>
       <td><?php echo $Lang['login']; ?></td>
       <td><input type="text" name="login" /></td>
    </tr>


    <tr>
      <td><?php echo $Lang['pass']; ?></td>
        <td><input type="password" name="password" /></td>

    </tr>
    <tr>
        <td><?php echo $Lang['checkpass']; ?></td>
        <td><input type="password" name="checkpassword" /></td>

    </tr>

    <tr>
        <td><?php echo $Lang['sex']; ?></td>
        <td><input type="radio" name="sex" value="Чоловік" id="radio1"> <label for="radio1">Чоловік</label>
        <input type="radio" name="sex" value="Жінка" id="radio2">  <label for="radio2">Жінка</label> </td>
    </tr>

    <tr>
        <td><?php echo $Lang['city']; ?></td>
        <td>
            <select  name="city">

                <option selected value="Житомир">Житомир</option>
                <option value="Київ">Київ</option>
                <option value="Вінниця">Вінниця</option>
            </select>
        </td>
    </tr>


    <tr>
        <td><?php echo $Lang['games']; ?></td>
        <td>
            <input name="games[]" type="checkbox" value="Футбол" id="football"> <label for="football">Футбол</label>
            <br>
            <input name="games[]" type="checkbox" value="Баскетбол" id="basketball"> <label for="basketball">Баскетбол</label>
            <br>
            <input name="games[]" type="checkbox" value="Воллейбол" id="valleyball"> <label for="valleyball">Воллейбол</label>

        </td>
    </tr>


    <tr>
        <td><?php echo $Lang['text']; ?></td>
        <td><textarea rows="4" cols="40" name="text"></textarea></td>
    </tr>

<tr>
    <td><?php echo $Lang['f']; ?></td>
    <td><input type="file" name="f"></td>
</tr>

    <tr>
        <td colspan="2"> <input type="submit" value="<?php echo $Lang['reg']; ?>"></td>
    </tr>

</table>
</form>

<form action="language.php">
<table >
    <tr>
    <td><a href="index.php?lang=ua">Українська</a></td>
    <td><a href="index.php?lang=ru">Русский</a></td>
    </tr>
</table>
</form>
</body>
</html>