function isIPAddress(){

    ip = prompt("Введіть текст:");
    var reg = /\b(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\.){3}(?:(?:2([0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9]))\b/ig;


    alert( ip.match(reg) );

}

function findRGBA() {

    rgba = prompt("Введіть текст:");
    var reg =  /rgba\x28([012]\d\d|\d\d{0,1})(\s*?,\s*?)([012]\d\d|\d\d{0,1})(\s*?,\s*?)([012]\d\d|\d\d{0,1})(\s*?,\s*?)(0?\.\d+|[01])\x29/g;


    alert( rgba.match(reg) );
}

function findHexColor(){
    hex = prompt("Введіть текст:");
    var reg = /^#?([0-9a-f]{3}){1,2}$/ig;


    alert( hex.match(reg) );
}

function findTags() {
    text = prompt("Введіть текст:");
    tag = prompt("Введіть тег:");

    var re = new RegExp("<" + tag + "(\\s|\\S)*?<\\/" + tag + ">", 'ig');



    alert( text.match(re) );
}

function findPosNum() {
    text = prompt("Введіть текст:");


    var re = /^(0*[1-9][0-9]*(\.[0-9]*)?|0*\.[0-9]*[1-9][0-9]*)$/gm ;



    alert( text.match(re) );
}

function  findDates(){
    data = prompt("Введіть текст:");
    var reg = /^(0?[1-9]|1[0-2])[\-](0?[1-9]|[12]\d|3[01])[\-](19|20)\d{2}$/;


    alert( data.match(reg) );
}
