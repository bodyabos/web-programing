var date = new Date();
var monthsUkr = ["січень", "лютий", "березень", "квітень", "травень", "червень", "липень", "серпень", "вересень", "жовтень", "листопад", "грудень"];
var weekDayUkr = ["", "неділя", "понеділок", "вівторок", "середа", "четвер", "п'ятниця", "субота"];
var weekDayRu = ["", "Воскресенье", "понедельник", "вторник", "среда", "четверг", "пятниця", "субота"];
var weekDayEn = ['', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var monthsEn = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"];
var monthsRu = ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентбрь", "октябрь", "ноябрь", "декабрь"];


function showdate() {
var elem = document.getElementById('1');
var showWeek = "День тижня: " + weekDayUkr[date.getDay() + 1];
var showDate = "Дата: " + date.getDate() + " " + monthsUkr[date.getMonth()] + " " + date.getFullYear() + " року";
elem.innerHTML = showDate + "<br>" + showWeek + "<br>" + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

}

function showday() {
    var elem = document.getElementById('2');

    elem.innerHTML = "День по рахунку: " + date.getDay() + "<br>" + "Назва дня: " + weekDayUkr[date.getDay() + 1];
}

function showdaysago() {
    var elem = document.getElementById('3');
    var day =   document.getElementById('daysAgo').value;

    date1 = Date.now() + (day * 24 * 3600 * 1000);
    date1 = new Date(date1);
    var showDate = "Дата: " + date1.getDate() + " " + monthsUkr[date1.getMonth()] + " " + date1.getFullYear() + " року";
    elem.innerHTML = showDate + "<br>";

}

function lastDayOfMonth()
{
    var elem = document.getElementById('4');
    var lastmonth =   document.getElementById('getmonth').value;
    var lastyear =   document.getElementById('getyear').value;

    date2 = new Date (lastyear, lastmonth, 0);

    var showDate = "Дата: " + date2.getDate() + " " + monthsUkr[date2.getMonth()] + " " + date2.getFullYear() + " року";
    elem.innerHTML = showDate + "<br>";

}

function seconds() {
    var elem = document.getElementById('5');

    var date = new Date();
    var secsfromstart = date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds();
    var secsfronend = 24 * 3600 + 60 * 60 + 60 - secsfromstart;

    var showDate = secsfromstart + "---" + secsfronend;
    elem.innerHTML = showDate + "<br>";

}

function day6func() {
    var elem = document.getElementById('6');
    elem.innerHTML = document.getElementById('day6').value +
        "." + document.getElementById('month6').value +
        "." + document.getElementById('year6').value;

    var date = new Date(document.getElementById('year6').value,  document.getElementById('month6').value , document.getElementById('day6').value );
   var showDate = date.getDay() + "." + date.getMonth() + "." + date.getFullYear();
    elem.innerHTML = showDate + "<br>";
}

function difer() {

    var elem = document.getElementById('7');

    var date1 = new Date(document.getElementById('data1').value);
    var date2 = new Date(document.getElementById('data2').value);
    var showDate = (date2 - date1) / (1000*3600*24);
    elem.innerHTML = showDate + "<br>";
}

function formatDate() {

    var elem = document.getElementById('8');
    date = new Date(document.getElementById('format').value);
    var diff = new Date() - date;

    if (diff < 1000) {
        elem.innerHTML = 'только что';
    }

    var sec = Math.floor(diff / 1000);

    if (sec < 60) {
        elem.innerHTML = sec + ' сек. назад';
    }

    var min = Math.floor(diff / 60000);
    if (min < 60) {
        elem.innerHTML = min + ' мин. назад';
    }


    var d = date;
    d = [
        '0' + d.getDate(),
        '0' + (d.getMonth() + 1),
        '' + d.getFullYear(),
        '0' + d.getHours(),
        '0' + d.getMinutes()
    ];

    for (var i = 0; i < d.length; i++) {
        d[i] = d[i].slice(-2);
    }

    elem.innerHTML = d.slice(0, 3).join('.') + ' ' + d.slice(3).join(':');

}

function dateukr() {

    var elem = document.getElementById('10');

    var date = new Date();
    var showDate =  weekDayUkr[ date.getDay() + 1] + ", " + date.getDate() + " " + monthsUkr[date.getMonth()] + " " + date.getFullYear() + " року нашої ери, " + + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

    elem.innerHTML = showDate + "<br>";

}

function dateru() {

    var elem = document.getElementById('10');

    var date = new Date();
    var showDate =  weekDayRu[ date.getDay() + 1] + ", " + date.getDate() + " " + monthsRu[date.getMonth()] + " " + date.getFullYear() + " года нашей эры, " + + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

    elem.innerHTML = showDate + "<br>";

}
function dateen() {

    var elem = document.getElementById('10');

    var date = new Date();
    var showDate =  weekDayEn[ date.getDay() + 1] + ", " + date.getDate() + " " + monthsEn[date.getMonth()] + " " + date.getFullYear() + " , " + + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

    elem.innerHTML = showDate + "<br>";

}