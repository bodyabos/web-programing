var cvs = document.getElementById('canvas');
var ctx = cvs.getContext('2d');



var bird = new Image();
var bg = new Image();
var fg = new Image();
var pipeNorth = new Image();
var pipeSouth = new Image();
var heart = new Image();



bird.src = "images/bird.png";
bg.src = "images/bg.png";
fg.src = "images/fg.png";
pipeNorth.src = "images/pipeNorth.png";
pipeSouth.src = "images/pipeSouth.png";
heart.src = "images/heart.png";






var bX = 300;
var bY = 200;

var gravity = 1.5;
var gravityX = 0.3;

function moveUp(){
    bY-= 35;
    bX+= gravityX*50;
}
function moveForward(){
    bX+=50;
}
function moveBack(){
    bX-=50;
}
function moveDown(){
    bY+=50;
}

document.addEventListener("keydown",  event =>
    {
        if(event.keyCode === 32) moveUp();
        if(event.keyCode === 39) moveForward();
        if(event.keyCode === 38) moveUp();
        if(event.keyCode === 40) moveDown();
        if(event.keyCode === 37) moveBack();

    });




 var score = 0;

var pipe = [];

pipe[0] = {
    x: cvs.width,
    y : 0
};

var gap = 55;
var constant = pipeNorth.height+gap;


var livesDraw = [];
var lifes = 3;

livesDraw[0] = {
    x: 0,
    y: 0
};
function drawLife() {
 livesDraw.push(
     {
         x: Math.floor(Math.random() * cvs.width) -
             cvs.width,
         y: Math.floor(Math.random() * cvs.height) -
             cvs.height
     }
 )
    ctx.drawImage(heart,  livesDraw[livesDraw.length-1].x,  livesDraw[livesDraw.length-1].y);

}


function draw() {





    var ptrn1 = ctx.createPattern(bg, 'repeat-x'); // Create a pattern with this image, and set it to "repeat".
    ctx.fillStyle = ptrn1;
    ctx.fillRect(0 , 0, cvs.width, cvs.height);
  //  ctx.drawImage(bg,0, 0);

    for(var i = 0 ; i <  pipe.length; i++ ){
        ctx.drawImage(pipeNorth, pipe[i].x, pipe[i].y);
        ctx.drawImage(pipeSouth, pipe[i].x, pipe[i].y + constant);


        pipe[i].x= pipe[i].x-2;

        if(   ( 1<= (bX - pipe[i].x) && (bX - pipe[i].x)  <= 10)) {
            score++;

        }

        if(pipe[i].x == 1000)
        {
            pipe.push(
                {
                    x: cvs.width,
                    y: Math.floor(Math.random()*pipeNorth.height) -
                        pipeNorth.height
                }
            )
        }

        if(bX + bird.width >= pipe[i].x && bX <= pipe[i].x + pipeNorth.width
        && (bY <= pipe[i].y + pipeNorth.height  || bY+bird.height>= pipe[i].y + constant )
        || bY + bird.height>= cvs.height - fg.height){
          lifes--;
          pipeNorth.width = 0;
          pipeSouth.height = 0;
//pipe[i].remove();
        }

       if(lifes == 0) location.reload();



    }


  /*  var ptrn = ctx.createPattern(fg, 'repeat'); // Create a pattern with this image, and set it to "repeat".
    ctx.fillStyle = ptrn;
    ctx.fillRect(0 , cvs.height - fg.height, cvs.width, fg.height);*/

   ctx.drawImage(fg, 0, cvs.height-fg.height );

    ctx.drawImage(bird, bX, bY);
    bY+= gravity;
    bX-= gravityX;



    ctx.fillStyle = "#FFF";
    ctx.font = "30px Verdana";
    ctx.fillText("Score: " + score , 20, cvs.height- 50 );

    for(i=0; i<lifes; i++)
    ctx.drawImage(heart,  i*30,  cvs.height - fg.height + 10);

    setInterval(drawLife(), 5000);
    requestAnimationFrame(draw)
}

draw();









