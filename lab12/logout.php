<?php
session_start();
require_once 'session.php';

Session::destroy();

header('Location: login.php');