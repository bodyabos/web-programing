<?php
require_once('RegistrationForm.php');
require_once('Password.php');
require_once('Session.php');

session_start();
if (Session::has('user')) {
    header('location: index.php');
}
$servername = "localhost";
$usernamedb = "root";
$passworddb = "";
$db = "lab_12";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$db", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

$form = new RegistrationForm($_POST);

if ($_POST) {
    if ($form->validate()) {
        $email = $form->getEmail();
        $username = $form->getUsername();
        $password = new Password($form->getPassword());
        $query = "SELECT * FROM users WHERE username = '{$username}'";
        $statement = $conn->prepare($query);
        $statement->execute();
        $result = $statement->fetchColumn();
        if ($result) {
            echo 'user with such name already exist';
        } else {
            $conn->query("INSERT INTO users (email, username, password) VALUES ('{$email}','{$username}','{$password}')");
            $user = $result['username'];
            Session::set('user', $username);
            header('location: index.php?msg=You have been logged in');
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register</title>
</head>
<body>
<form method="post" id="form">
    Email: <input type="email" name="email" value="<?= $form->getEmail(); ?>" required/> <br/><br/>
    Username: <input type="text" name="username" value="<?= $form->getUsername(); ?>" required/> <br/><br/>
    Password: <input type="password" name="password" required/> <br/><br/>
    Confirm password: <input type="password" name="passwordConfirm"/> <br/><br/>
    <input type="submit"/>
</form>
</body>
</html>