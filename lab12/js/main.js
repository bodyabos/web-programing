let submitButton = document.querySelector("#submit");
if (submitButton) {
    submitButton.addEventListener("click", submitted);
}
if (document.location.pathname === "/index.php") {
    function submitted(event) {
        let login = document.querySelector("input[name='login']");
        let password = document.querySelector("input[name='password']");
        let repeated_password = document.querySelector("input[name='repeated_password']");
        let sex = document.querySelector("input[name='sex']:checked");
        let flag = false;
        if (login.value === "") {
            login.style.border = "1px solid red";
            let error = login.parentElement.querySelector(".error");
            error.innerText = "Enter login please";
            flag = true;
        } else {
            login.style.border = "1px solid lightgray";
            let error = login.parentElement.querySelector(".error");
            error.innerText = "";
        }
        if (password.value === "") {
            password.style.border = "1px solid red";
            let error = password.parentElement.querySelector(".error");
            error.innerText = "Enter password please";
            flag = true;
        } else {
            password.style.border = "1px solid lightgray";
            let error = password.parentElement.querySelector(".error");
            error.innerText = "";
        }
        if (repeated_password.value === "") {
            repeated_password.style.border = "1px solid red";
            let error = repeated_password.parentElement.querySelector(".error");
            error.innerText = "Enter password once more please";
            flag = true;
        } else {
            repeated_password.style.border = "1px solid lightgray";
            let error = repeated_password.parentElement.querySelector(".error");
            error.innerText = "";
        }
        if (password.value !== repeated_password.value) {
            password.style.border = "1px solid orange";
            repeated_password.style.border = "1px solid orange";
            let error = repeated_password.parentElement.querySelector(".error");
            error.innerText = "Passwords do not match";
            flag = true;
        } else {
            password.style.border = "1px solid lightgray";
            repeated_password.style.border = "1px solid lightgray";
            let error = repeated_password.parentElement.querySelector(".error");
            error.innerText = "";
        }
        if (!sex) {
            flag = true;
        }

        if (flag) {
            event.preventDefault();
        }
    }
}

if (document.location.pathname === "/processor.php" || document.location.pathname === "/login.php") {
    let elements = Array.from(document.querySelectorAll("span"));
    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener("click", input);
    }
    let btnGo = document.querySelector("button");
    if (btnGo)
        btnGo.addEventListener("click", send);
    let btnDel = document.querySelector(".del");
    if (btnDel)
        btnDel.addEventListener("click", deletePr);
    let exit = document.querySelector(".exit");
    if (exit)
        exit.addEventListener("click", exitf);

    let prevElement;

    function input(e) {
        let parentElement = e.target.parentElement;
        if (prevElement) {
            let input = prevElement.querySelector("input");
            console.log(input);
            send(input, true);
        }
        parentElement.innerHTML += "<input type='text'/>";
        prevElement = parentElement;
    }

    function send(input, generated) {
        if (!generated) {
            console.log("NOT GENERATED");
            input = document.querySelector("input");
        }
        if (!input) {
            alert("Нет поля");
        }
        if (input) {
            if (input.value !== "") {
                let php = document.querySelector(".php");
                let name = input.parentElement.querySelector("span").getAttribute("name");
                let login = input.parentElement.parentElement.parentElement.querySelector("span[name='login']").innerText;
                /*php.innerHTML = `<?php
                $rows = $pdo->exec(\"UPDATE \`users\`
                                        SET ${name}=${input.value}
                                        WHERE login like '${login}'\");
                ?>`;*/
                // 1. Создаём новый объект XMLHttpRequest
                var xmlhttp = new XMLHttpRequest();
                // var str = `$rows = $pdo->exec("UPDATE 'users' SET ${name}=${input.value} WHERE login like '${login}'");`;
                var url = "processor.php";
                var params = `code=1&name=${name}&inputValue=${input.value}&login=${login}`;
                xmlhttp.open("POST", url, true);

//Send the proper header information along with the request
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.setRequestHeader("Content-length", params.length);
                xmlhttp.setRequestHeader("Connection", "close");

                xmlhttp.onreadystatechange = function () {//Call a function when the state changes.
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        // alert(xmlhttp.responseText);
                    }
                };
                xmlhttp.send(params);

// 4. Если код ответа сервера не 200, то это ошибка
                if (xmlhttp.status != 200) {
                    // обработать ошибку
                    // alert(xmlhttp.status + ': ' + xmlhttp.statusText); // пример вывода: 404: Not Found
                } else {
                    // вывести результат
                    // alert(xmlhttp.responseText); // responseText -- текст ответа.
                }
            }
            input.parentElement.querySelector("span").innerText = input.value;
            input.parentNode.removeChild(input);
        }
    }

    function deletePr() {
        var xmlhttp = new XMLHttpRequest();
        // var str = `$rows = $pdo->exec("UPDATE 'users' SET ${name}=${input.value} WHERE login like '${login}'");`;
        var url = "processor.php";
        var params = `code=2`;
        xmlhttp.open("POST", url, true);

//Send the proper header information along with the request
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("Content-length", params.length);
        xmlhttp.setRequestHeader("Connection", "close");

        xmlhttp.onreadystatechange = function () {//Call a function when the state changes.
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                // alert(xmlhttp.responseText);
            }
        };
        xmlhttp.send(params);

// 4. Если код ответа сервера не 200, то это ошибка
        if (xmlhttp.status != 200) {
            // обработать ошибку
            // alert(xmlhttp.status + ': ' + xmlhttp.statusText); // пример вывода: 404: Not Found
        } else {
            // вывести результат
            // alert(xmlhttp.responseText); // responseText -- текст ответа.
        }
        setTimeout(function () {
            window.location = "index.php";
        }, 500);
    }
}

function exitf() {
    var xmlhttp = new XMLHttpRequest();
    // var str = `$rows = $pdo->exec("UPDATE 'users' SET ${name}=${input.value} WHERE login like '${login}'");`;
    var url = "processor.php";
    var params = `code=5`;
    xmlhttp.open("POST", url, true);

//Send the proper header information along with the request
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("Content-length", params.length);
    xmlhttp.setRequestHeader("Connection", "close");

    xmlhttp.onreadystatechange = function () {//Call a function when the state changes.
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            // alert(xmlhttp.responseText);
        }
    };
    xmlhttp.send(params);

// 4. Если код ответа сервера не 200, то это ошибка
    if (xmlhttp.status != 200) {
        // обработать ошибку
        // alert(xmlhttp.status + ': ' + xmlhttp.statusText); // пример вывода: 404: Not Found
    } else {
        // вывести результат
        // alert(xmlhttp.responseText); // responseText -- текст ответа.
    }
    setTimeout(function () {
        window.location = "index.php";
    }, 500);
}