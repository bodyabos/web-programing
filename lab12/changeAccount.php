<?php
$servername = "localhost";
$usernamedb = "root";
$passworddb = "";
$db = "lab_12";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$db", $usernamedb, $passworddb);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
require_once('Session.php');
session_start();

$currnetusername = Session::get('user');

$query = "SELECT * FROM users WHERE username = '{$currnetusername}'";
$result = $conn->query($query);
while ($res = $result->fetch(PDO::FETCH_NUM)) {
    $id = $res[0];
    $email = $res[1];
    $username = $res[2];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Change data</title>
</head>
<body>
<form method="post" action="updatepost.php">
    Email: <input type="email" name="email" value="<?= $email ?>" required/> <br/><br/>
    Username: <input type="text" name="username" value="<?= $username; ?>" required/> <br/><br/>
    <input type="hidden" value="<?= $id;?>" name="id">
    <input type="submit"/>
</form>
</body>
</html>