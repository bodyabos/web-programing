<?php
session_start();
$host = '127.0.0.1';
$db = 'lab12';
$user = 'root';
$pass = 'root';
$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);
$flag = false;
if (isset($_POST['code'])) {
    if ($_POST['code'] == 1) {
        $name = $_POST['name'];
        $iV = $_POST['inputValue'];
        $login = $_POST['login'];
        $rows = $pdo->exec("UPDATE `users` SET " . $name . " = '" . $iV . "' WHERE login = '" . $login . "'");
    }
    if ($_POST['code'] == 2) {
        $rows = $pdo->exec("DELETE FROM `users` WHERE login = '" . $_SESSION['login'] . "'");
        unset($_SESSION['login']);
        unset($_SESSION['password']);
        session_destroy();
        // PHP permanent URL redirection
//        header("Location: http://lab12/index.php", true, 301);
//        exit();
    }
    if ($_POST['code'] == 5) {
        unset($_SESSION['login']);
        unset($_SESSION['password']);
        session_destroy();
    }
    $flaglogin = false;
    if ($_POST['code'] == 3) {
        try {
            $login = $_POST['login'];
            $password = $_POST['password'];
            $result = $pdo->query("SELECT login FROM users");
            $existenceFlag = false;
            while ($row = $result->fetch()) {
                if ($login == $row['login']) {
                    $result = $pdo->query("SELECT password FROM users WHERE login = '" . $login . "'");
                    while ($row = $result->fetch()) {
                        if ($password == $row['password']) {
                            $flaglogin = true;
                            $result = $pdo->query("SELECT sex FROM users WHERE login = '" . $login . "'");
                            $existenceFlag = false;
                            while ($row = $result->fetch()) {
                                $sex = $row['sex'];
                                break;
                            }
                        }
                    }
                }
            }
        } catch (PDOException $e) {
            die("Error: " . $e->getMessage());
        }
    }
} else {
    if (isset($_POST['login']) && $_POST['login'] !== "") {
        $login = htmlentities($_POST['login']);
        $_SESSION['login'] = $login;
    } else {
        if (isset($_SESSION['login']))
            $login = htmlentities($_SESSION['login']);
        else {
            $login = "No login";
            $flag = true;
        }
    }
    if (isset($_POST['password']) && $_POST['password'] !== "") {
        $password = htmlentities($_POST['password']);
        $_SESSION['password'] = $password;
    } else {
        if (isset($_SESSION['login']))
            $password = htmlentities($_SESSION['password']);
        else {
            $password = "No password";
            $flag = true;
        }
    }
    if (isset($_POST['repeated_password']) && $_POST['repeated_password'] !== "") {
        $repeated_password = htmlentities($_POST['repeated_password']);
        $_SESSION['repeated_password'] = $repeated_password;
    } else {
        $repeated_password = "No repeated password";
        $flag = true;
    }
    if (isset($_POST['sex']) && $_POST['sex'] !== "") {
        $sex = htmlentities($_POST['sex']);
        if ($sex == "man") {
            $sex = 1;
        } else {
            $sex = 0;
        }
        $_SESSION['sex'] = $sex;
    } else {
        $sex = "No sex";
        $flag = true;
    }
    if ($flag) {
        echo "<script>alert('ERROOOOOOOOOOOOOR')</script>";
    } else {

//        $stmt = $pdo->query('SELECT login FROM users');
//        while ($row = $stmt->fetch()) {
//            echo $row['name'] . "\n";
//        }
//        echo "<script>alert('login $login')</script>";
        if (!$flaglogin)
            try {
                $result = $pdo->query("SELECT login FROM users");
                $existenceFlag = false;
                while ($row = $result->fetch()) {
                    if ($login == $row['login']) {
                        $existenceFlag = true;
                        echo "<script>alert('Такой пользователь уже есть'); 
                       window.location = \"index.php\";
                   </script>";
                    }
//            print_r($row);
                }
//            echo "<script>alert('flag $existenceFlag')</script>";
                // вставляем несколько строк в таблицу из прошлого примера
                echo "<script>alert('array $login, $password, $sex')</script>";
                if (!$existenceFlag) {
                    $rows = $pdo->exec("INSERT INTO `users` VALUES (null, '$login', '$password', '$sex')");
                }
                // в случае ошибки SQL выражения выведем сообщене об ошибке
                $error_array = $pdo->errorInfo();

                if ($pdo->errorCode() != 0000)
                    echo "SQL ошибка: " . $error_array[2] . '<br />';

                // теперь выберем несколько строчек из базы
                $result = $pdo->query("SELECT * FROM `users` ");
                // в случае ошибки SQL выражения выведем сообщене об ошибке
                $error_array = $pdo->errorInfo();
                if ($pdo->errorCode() != 0000)
                    echo "SQL ошибка: " . $error_array[2] . '<br /><br />';
                // теперь получаем данные из класса PDOStatement
                /*while ($row = $result->fetch()) {
                    // в результате получаем ассоциативный массив
                    print_r($row);
                }*/
            } catch (PDOException $e) {
                die("Error: " . $e->getMessage());
            }
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lab12 - Registration</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="wrapper">
    <h1>Лабораторна робота №12 - Дані з форми</h1>
    <table>
        <tr>
            <td>Логин:</td>
            <td>
                <span name="login"><?php echo $login ?></span>
            </td>
        </tr>
        <tr>
            <td>Пароль:</td>
            <td><span name="password"><?php echo $password ?></span></td>
        </tr>
        <!--        <tr>-->
        <!--            <td>Пароль (еще раз):</td>-->
        <!--            <td><span>--><?php //echo $repeated_password ?><!--</span></td>-->
        <!--        </tr>-->
        <tr>
            <td>Пол:</td>
            <td><span name="sex"><?php echo $sex ?></span></td>
        </tr>
        <tr>
            <td>
                <button>go</button>
            </td>
            <td>
                <button class="del">delete</button>
                <button class="exit">exit</button>
            </td>
        </tr>
    </table>
    <div class="php"></div>
</div>
<script src="js/main.js"></script>
</body>
</html>