<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lab12</title>
    <!--<base href="http://" target="_blank">-->
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="wrapper">
    <h1>Лабораторна робота №12</h1>
    <form enctype="multipart/form-data" method="post" action="processor.php">
        <table>
            <tr>
                <td>Логін:</td>
                <td><input type="text" name="login"><span class="error"></span>
                </td>
            </tr>
            <tr>
                <td>Пароль:</td>
                <td><input type="password" name="password"><span class="error"></span></td>
            </tr>
            <tr>
                <td>Пароль (повторно):</td>
                <td><input type="password" name="repeated_password"><span class="error"></span></td>
            </tr>
            <tr>
                <td>Стать:</td>
                <td>
                    <label><input type="radio" name="sex" value="man">М</label>
                    <label><input type="radio" name="sex" value="woman">Ж</label>
                    <span class="error"></span>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input id="submit" type="submit" value="Зареєструватись"> или <a href="login.php">Війти</a></td>
            </tr>
        </table>
    </form>
</div>
<script src="js/main.js"></script>
</body>
</html>
